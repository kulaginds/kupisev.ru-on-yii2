<?php

/* @var $this yii\web\View */
/* @var $id integer */
/* @var $contact_name string */
/* @var $edit_hash string */
/* @var $delete_hash string */

use yii\helpers\Url;

?>
<h1><?= Yii::$app->name ?></h1>
<p>Здравствуйте, <?php echo $contact_name; ?>!</p>
<p>Благодарим за создание объявления.</p>
<p>Управление объявлением:</p>
<ul>
    <li><a href="<?= Url::to(['advert/index', 'id' => $id], true) ?>">Просмотр</a></li>
    <li><a href="<?= Url::to(['advert/edit', 'id' => $id, 'edit_hash' => $edit_hash], true) ?>">Редактировать</a></li>
    <li><a href="<?= Url::to(['advert/delete', 'id' => $id, 'delete_hash' => $delete_hash], true) ?>">Удалить</a></li>
</ul>