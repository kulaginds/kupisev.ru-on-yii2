<?php

return [
    'Phone number does not seem to be a valid phone number' => 'Некорректный формат телефона',
    'Unexpected Phone Number Format' => 'Некорректный формат телефона',
    'Unexpected Phone Number Format or Country Code' => 'Некорректный формат телефона',
];
