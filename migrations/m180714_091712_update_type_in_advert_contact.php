<?php

use yii\db\Migration;
use app\models\AdvertContact;

/**
 * Class m180714_091712_update_type_in_advert_contact
 */
class m180714_091712_update_type_in_advert_contact extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(AdvertContact::tableName(), 'type',
            $this->enum(AdvertContact::TYPES) . ' NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180714_091712_update_type_in_advert_contact cannot be reverted.\n";

        return false;
    }

    protected function enum($data)
    {
        $result = 'ENUM(';

        foreach ($data as $item) {
            $result .= sprintf("'%s',", $item);
        }

        $result = substr($result, 0, strlen($result) - 1);

        $result .= ')';

        return $result;
    }
}
