<?php

use yii\db\Migration;
use app\models\Advert;
use app\models\Category;

/**
 * Class m180627_072041_create_table_advert
 */
class m180627_072041_create_table_advert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Advert::tableName(), [
            'id' => $this->primaryKey(10),
            'title' => $this->string(60)->notNull(),
            'description' => $this->text()->notNull(),
            'cost' => $this->integer(10)->notNull()->unsigned(),
            'category_id' => $this->smallInteger(5)->notNull()->unsigned(),
            'edit_hash' => $this->binary(16)->defaultValue(null),
            'delete_hash' => $this->binary(16)->defaultValue(null),
            'contact_name' => $this->string(40)->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Advert::tableName(), 'id',
            $this->integer(10)->notNull()->unsigned() . ' AUTO_INCREMENT');
        $this->addForeignKey('fk_advert_category', Advert::tableName(),
            'category_id', Category::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180627_072041_create_table_advert cannot be reverted.\n";

        $this->dropForeignKey('fk_advert_category', Advert::tableName());
        $this->dropTable(Advert::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_072041_create_table_advert cannot be reverted.\n";

        return false;
    }
    */
}
