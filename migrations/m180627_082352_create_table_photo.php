<?php

use yii\db\Migration;
use app\models\Photo;
use app\models\Advert;

/**
 * Class m180627_082352_create_table_photo
 */
class m180627_082352_create_table_photo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Photo::tableName(), [
            'id' => $this->primaryKey(10),
            'advert_id' => $this->integer(10)->notNull()->unsigned(),
            'name' => $this->binary(16)->notNull(),
            'created_at' => $this->dateTime()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Photo::tableName(), 'id',
            $this->integer(10)->notNull()->unsigned() . ' AUTO_INCREMENT');
        $this->addForeignKey('fk_photo_advert', Photo::tableName(),
            'advert_id', Advert::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180627_082352_create_table_photo cannot be reverted.\n";

        $this->dropForeignKey('fk_photo_advert', Photo::tableName());
        $this->dropTable(Photo::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_082352_create_table_photo cannot be reverted.\n";

        return false;
    }
    */
}
