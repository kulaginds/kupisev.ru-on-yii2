<?php

use yii\db\Migration;
use app\models\User;

/**
 * Handles the creation of table `user`.
 */
class m180713_174834_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(User::tableName(), [
            'id' => $this->primaryKey(10),
            'email' => $this->string(30)->notNull(),
            'password_hash' => $this->string(60)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(User::tableName(), 'id',
            $this->integer(10)->notNull()->unsigned() . ' AUTO_INCREMENT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(User::tableName());
    }
}
