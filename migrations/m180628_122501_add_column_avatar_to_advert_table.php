<?php

use yii\db\Migration;
use app\models\Advert;

/**
 * Class m180628_122501_add_column_avatar_to_advert_table
 */
class m180628_122501_add_column_avatar_to_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Advert::tableName(), 'avatar',
            $this->binary(16)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180628_122501_add_column_avatar_to_advert_table cannot be reverted.\n";

        $this->dropColumn(Advert::tableName(), 'avatar');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180628_122501_add_column_avatar_to_advert_table cannot be reverted.\n";

        return false;
    }
    */
}
