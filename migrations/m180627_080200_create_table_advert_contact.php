<?php

use yii\db\Migration;
use app\models\AdvertContact;
use app\models\Advert;

/**
 * Class m180627_080200_create_table_advert_contact
 */
class m180627_080200_create_table_advert_contact extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(AdvertContact::tableName(), [
            'id' => $this->primaryKey(10),
            'advert_id' => $this->integer(10)->notNull()->unsigned(),
            'type' => $this->enum(AdvertContact::TYPES) . ' NOT NULL',
            'value' => $this->string(40)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(AdvertContact::tableName(), 'id',
            $this->integer(10)->notNull()->unsigned() . ' AUTO_INCREMENT');
        $this->addForeignKey('fk_advert_contact_advert', AdvertContact::tableName(),
            'advert_id', Advert::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    protected function enum($data)
    {
        $result = 'ENUM(';

        foreach ($data as $item) {
            $result .= sprintf("'%s',", $item);
        }

        $result = substr($result, 0, strlen($result) - 1);

        $result .= ')';

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180627_080200_create_table_advert_contact cannot be reverted.\n";

        $this->dropForeignKey('fk_advert_contact_advert', AdvertContact::tableName());
        $this->dropTable(AdvertContact::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_080200_create_table_advert_contact cannot be reverted.\n";

        return false;
    }
    */
}
