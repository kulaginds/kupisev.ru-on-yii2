<?php

use yii\db\Migration;
use app\models\Category;

/**
 * Class m180627_065748_create_table_category
 */
class m180627_065748_create_table_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Category::tableName(), [
            'id' => $this->primaryKey(10),
            'parent_id' => $this->smallInteger(5)->notNull()->unsigned()
                ->defaultValue(0),
            'name' => $this->string(30)->notNull(),
            'hidden' => $this->boolean()->defaultValue(false),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Category::tableName(), 'id',
            $this->smallInteger(5)->notNull()->unsigned() . ' AUTO_INCREMENT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180627_065748_create_table_category cannot be reverted.\n";

        $this->dropTable(Category::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_065748_create_table_category cannot be reverted.\n";

        return false;
    }
    */
}
