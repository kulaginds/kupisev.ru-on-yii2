<?php

use yii\db\Migration;
use app\models\Advert;

/**
 * Class m180713_163637_add_column_checked_to_advert_table
 */
class m180713_163637_add_column_checked_to_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Advert::tableName(), 'checked',
            $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180713_163637_add_column_checked_to_advert_table cannot be reverted.\n";

        $this->dropColumn(Advert::tableName(), 'checked');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180713_163637_add_column_checked_to_advert_table cannot be reverted.\n";

        return false;
    }
    */
}
