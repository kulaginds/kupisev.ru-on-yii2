<?php

namespace app\commands;

use Yii;
use app\models\Advert;
use yii\console\Controller;
use yii\db\Expression;

class AdvertController extends Controller
{
    /**
     * Автоматически удаляет объявления старше 7 дней.
     */
    public function actionAutoremove()
    {
        $adverts = Advert::find()
            ->where([
                '<=', 'created_at',
                new Expression('NOW() - INTERVAL '
                    . Yii::$app->params['advertLifetime'] . ' DAY')
            ])
            ->all();

        foreach ($adverts as $advert) {
            $advert->delete();
        }
    }

    /**
     * Удаляет объявление по id.
     *
     * @param $id
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $advert = Advert::findOne($id);

        if (is_null($advert)) {
            return;
        }

        $advert->delete();
    }
}