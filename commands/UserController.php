<?php

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\BaseConsole;

class UserController extends Controller
{
    /**
     * Создаёт нового пользователя-администратора.
     */
    public function actionCreate($password = null)
    {
        $email = BaseConsole::input('Введите e-mail: ');

        if (is_null($password)) {
            $password = Yii::$app->getSecurity()->generateRandomString(10);
        }

        $user = new User();
        $user->email = $email;
        $user->setPassword($password);

        if (!$user->save()) {
            print 'Не удалось создать пользователя.' . "\n";

            return ExitCode::UNSPECIFIED_ERROR;
        }

        print 'Email: ' . $email . "\n";
        print 'Пароль: ' . $password . "\n";

        return ExitCode::OK;
    }
}