# KupiSev.ru
Open source engine for advert board site

## Installation
Configure config/-sample.php files.
Make:
```bash
composer install
npm i
./node_modules/.bin/gulp
cp web/index-dev.php web/index.php
./yii migrate
./yii migrate --migrationPath=@vendor/nterms/yii2-mailqueue/migrations/
```

## Make admin user
```bash
./yii user/create
```

## Demo data
```bash
./yii fixture "Category,Advert" --namespace="tests\fixtures"
```

Admin panel: /admin.

## nginx
nginx.conf:
```
user  www-data;
```

## For HTTPS
```
fastcgi_param HTTPS on;
```

## PHP config
```
short_open_tag = On
expose_php = Off
allow_url_include = Off
cgi.fix_pathinfo = 0
```

## PHP extensions
```
php7.1
php7.1-fpm
php7.1-cli
php7.1-curl
php7.1-gd
php7.1-mbstring
php7.1-intl
php7.1-pdo
php7.1-pdo-mysql
php7.1-xml
php7.1-zip
```

## cURL
```
*/5 * * * * ./yii mailqueue/process
0 4 * * * ./yii advert/autoremove
```
