<?php

namespace app\models;

use yii\db\ActiveRecord;

/** @property int $id */
/** @property int $advert_id */
/** @property string $type */
/** @property string $value */

class AdvertContact extends ActiveRecord
{
    const TYPE_EMAIL = 'email';
    const TYPE_PHONE = 'phone';
    const TYPE_VK = 'vk';
    const TYPES = [self::TYPE_EMAIL, self::TYPE_PHONE, self::TYPE_VK];

    public function rules()
    {
        return [
            [['advert_id', 'type', 'value'], 'required'],
            [['id', 'advert_id'], 'integer', 'min' => 0],
            ['type', 'in', 'range' => self::TYPES],
            ['value', 'string', 'max' => 40],
        ];
    }
}