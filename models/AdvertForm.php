<?php

namespace app\models;

use function foo\func;
use Yii;
use himiklab\yii2\recaptcha\ReCaptchaValidator;

class AdvertForm extends AbstractForm
{
    public $id;
    public $title;
    public $cost;
    public $category_id;
    public $description;
    public $contact_name;
    public $contact_email;
    public $contact_phone;
    public $contact_vk;
    public $reCaptcha;

    private $_advert;

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок объявления',
            'cost' => 'Стоимость в рублях',
            'category_id' => 'Категория объявления',
            'description' => 'Описание объявления',
            'contact_name' => 'Как вас зовут?',
            'contact_email' => 'Контактный E-mail',
            'contact_phone' => 'Контактный телефон',
            'contact_vk' => 'Ссылка в VK',
        ];
    }

    public function rules()
    {
        return [
            [['title', 'cost', 'category_id', 'description',
                'contact_name'], 'required'],
            [['contact_email', 'contact_phone'], 'required',
                'on' => ['create', 'edit']],
            [['title', 'cost', 'description', 'contact_name',
                'contact_email', 'contact_phone'], 'trim'],
            ['title', 'string', 'max' => 60],
            ['contact_email', 'email'],
            ['contact_phone', 'udokmeci\yii2PhoneValidator\PhoneValidator',
                'country' => 'RU'],
            [['id', 'cost'], 'integer', 'min' => 0],
            ['category_id', 'checkCategoryId', 'params' => ['strict' => true]],
            [['reCaptcha'], ReCaptchaValidator::class,
                'uncheckedMessage' => 'Похоже вы робот :) Нажмите на галочку ещё раз',
                'on' => ['create']],
            ['contact_vk', 'url'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $advert = $this->getAdvert();
        $advert->title = $this->title;
        $advert->cost = $this->cost;
        $advert->category_id = $this->category_id;
        $advert->description = $this->description;
        $advert->contact_name = $this->contact_name;

        if (empty($this->id)) {
            $advert->edit_hash = bin2hex(random_bytes(16));
            $advert->delete_hash = bin2hex(random_bytes(16));
            $advert->checked = Yii::$app->params['checkAfterCreate'];
        }

        if (!$advert->save()) {
            return false;
        }

        if (is_null($advert->avatar)) {
            $photos = $advert->getPhotos();

            if (count($photos) > 0) {
                $advert->avatar = $photos[0]->name;
            }

            $advert->save();
        }

        $id = $advert->id;
        $contact_name = $advert->contact_name;
        $edit_hash = $advert->edit_hash;
        $delete_hash = $advert->delete_hash;

        if (!empty($this->contact_email)) {
            $email = $advert->getEmail();
            $email->value = $this->contact_email;

            if (!$email->save()) {
                return false;
            }
        }

        if (!empty($this->contact_phone)) {
            $phone = $advert->getPhone();
            $phone->value = $this->contact_phone;

            if (!$phone->save()) {
                return false;
            }
        }

        if (!empty($this->contact_vk)) {
            $vk = $advert->getVk();
            $vk->value = $this->contact_vk;

            if (!$vk->save()) {
                return false;
            }
        }

        if (is_null($this->id)) {
            /** @var \nterms\mailqueue\Message $mail */
            $mail = Yii::$app->mailqueue->compose('create_advert', compact(
                'id',
                'contact_name',
                'edit_hash',
                'delete_hash'
            ));

            $mail->setFrom(Yii::$app->params['noreplyEmail'])
                 ->setTo($this->contact_email)
                 ->setSubject('Управление созданным объявлением');

            return $mail->queue();
        }

        return true;
    }

    public function saveAndCheck()
    {
        if (!$this->save()) {
            return false;
        }

        $advert = $this->getAdvert();

        $advert->checked = true;

        return $advert->save();
    }

    /** @var \app\models\Advert $advert */
    public function setAdvert($advert)
    {
        $this->_advert = $advert;
        $this->id = $advert->id;
        $this->title = $advert->title;
        $this->cost = $advert->cost;
        $this->category_id = $advert->category_id;
        $this->description = $advert->description;
        $this->contact_name = $advert->contact_name;
        $this->contact_email = $advert->getEmail()->value;
        $this->contact_phone = $advert->getPhone()->value;
    }

    public function getAdvert()
    {
        if (empty($this->_advert)) {
            $this->_advert = Advert::findOne($this->id);

            if (is_null($this->_advert)) {
                $this->_advert = new Advert();
            } else {
                $this->id = $this->_advert->id;
            }
        }

        return $this->_advert;
    }
}