<?php

namespace app\models;

use igogo5yo\uploadfromurl\UploadFromUrl;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

define('ORIGINAL_SIZE', 600);
define('THUMBNAIL_SIZE', 300);
define('JPEG_QUALITY', 75);

class PhotoForm extends Model
{
    public $id;
    /** @var $imageFile UploadedFile|UploadFromUrl */
    public $imageFile;
    /** @var $_photo Photo */
    private $_photo;

    public function rules()
    {
        return [
            ['imageFile', 'image', 'skipOnEmpty' => false,
                'extensions' => 'png, jpg, gif', 'on' => ['file']],
            ['imageFile', 'igogo5yo\uploadfromurl\FileFromUrlValidator', 'on' => ['url']],
        ];
    }

    public function upload()
    {
        if (!$this->validate()) {
            return false;
        }

        $name = bin2hex(random_bytes(16));
        $newname = Yii::$app->params['uploadsPath'] . $name;
        $full = $newname . '.jpg';
        $thumb = $newname . '_thumb.jpg';

        if ($this->getScenario() == 'file') {
            $source = $this->imageFile->tempName;
        } else {
            $source = $this->uploadFromUrl($this->imageFile->url,
                $this->imageFile->baseName);
        }

        if (is_null($source)) {
            return false;
        }

        if (!$this->handleImage($source, $full, $thumb)) {
            $this->addError('imageFile', 'Неизвестный формат файла.');
            return false;
        }

        if (empty($this->id)) {
            return false;
        }

        $this->_photo = new Photo();
        $this->_photo->advert_id = $this->id;
        $this->_photo->name = $name;

        return $this->_photo->save();
    }

    public function getPhoto()
    {
        return $this->_photo;
    }

    protected function uploadFromUrl($url, $baseName)
    {
        $photo = file_get_contents($url);
        $tmp = '/tmp/' . $baseName;

        if (!file_put_contents($tmp, $photo)) {
            return null;
        }

        return $tmp;
    }

    protected function handleImage($filename, $original_url, $thumbnail_url)
    {
        list($width, $height, $type) = getimagesize($filename);

        // need for resize
        $image_width  = $width;
        $image_height = $height;

        $x = $y = 0;

        $rectangle = true;

        // calculations
        if ($width > $height) {
            // album orientation
            $ratio = $width / ORIGINAL_SIZE;

            $width  = ORIGINAL_SIZE;
            $height = ceil($height / $ratio);

            $y = ceil((ORIGINAL_SIZE - $height) / 2);
        } elseif ($width < $height) {
            // portrait orientation
            $ratio = $height / ORIGINAL_SIZE;

            $width  = ceil($width / $ratio);
            $height = ORIGINAL_SIZE;

            $x = ceil((ORIGINAL_SIZE - $width) / 2);
        } else {
            $width  = ORIGINAL_SIZE;
            $height = ORIGINAL_SIZE;

            $rectangle = false;
        }

        // opening file
        switch ($type) {
            case IMAGETYPE_GIF:
                $img = imagecreatefromgif($filename);
                break;
            case IMAGETYPE_JPEG:
                $img = imagecreatefromjpeg($filename);
                break;
            case IMAGETYPE_PNG:
                $img = imagecreatefrompng($filename);
                break;
            default:
                return false;
                break;
        }

        // resized image
        $new_img = imagecreatetruecolor($width, $height);

        // resize image
        imagecopyresampled($new_img, $img, 0, 0, 0, 0, $width, $height, $image_width, $image_height);
        imagedestroy($img);

        $original_img = $new_img;

        if ($rectangle) {
            // original
            $original_img = imagecreatetruecolor(ORIGINAL_SIZE, ORIGINAL_SIZE);

            // white
            imagefill($original_img, 0, 0, imagecolorallocate($original_img, 255, 255, 255));

            // merge image with original
            imagecopy($original_img, $new_img, $x, $y, 0, 0, $width, $height);

            imagedestroy($new_img);
        }

        $thumbnail_img = imagecreatetruecolor(THUMBNAIL_SIZE, THUMBNAIL_SIZE);

        // resize original to thumbnail
        imagecopyresampled($thumbnail_img, $original_img, 0, 0, 0, 0, THUMBNAIL_SIZE, THUMBNAIL_SIZE, ORIGINAL_SIZE, ORIGINAL_SIZE);

        // save original and thumbnail
        imagejpeg($original_img, $original_url, JPEG_QUALITY);
        imagejpeg($thumbnail_img, $thumbnail_url, JPEG_QUALITY);

        return true;
    }
}