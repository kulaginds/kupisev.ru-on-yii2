<?php

namespace app\models;

use yii\db\ActiveQuery;

class SearchForm extends AbstractForm
{
    public $query;
    public $category_id;

    public function attributeLabels()
    {
        return [
            'query' => 'Поисковой запрос',
            'category_id' => 'Категория',
        ];
    }

    public function rules()
    {
        return [
            ['query', 'string'],
            ['category_id', 'integer', 'min' => 0],
            ['category_id', 'checkCategoryId'],
        ];
    }

    /**
     * @param $query ActiveQuery
     */
    public function search($query)
    {
        if (!$this->validate()) {
            return;
        }

        $this->handleQuery($query);
        $this->handleCategoryId($query);
        $query->andWhere(['checked' => true]);
    }

    /**
     * @param $query ActiveQuery
     */
    protected function handleQuery($query)
    {
        if (empty($this->query)) {
            return;
        }

        $query->andWhere(
            ['like', 'title', $this->query]
        );

        $query->orWhere(
            ['like', 'description', $this->query]
        );
    }

    /**
     * @param $query ActiveQuery
     */
    protected function handleCategoryId($query)
    {
        if (empty($this->category_id)) {
            return;
        }

        $query->andWhere(
            ['category_id' => $this->category_id]
        );
    }
}
