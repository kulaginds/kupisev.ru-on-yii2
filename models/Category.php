<?php

namespace app\models;

use yii\db\ActiveRecord;

/** @property int $id */
/** @property int $parent_id */
/** @property string $name */
/** @property boolean $hidden */

class Category extends ActiveRecord
{
    private static $_selectDataProvider;

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id', 'parent_id'], 'integer', 'min' => 0],
            ['name', 'string', 'max' => 30],
            ['hidden', 'boolean'],
        ];
    }

    public static function selectDataProvider()
    {
        if (empty(self::$_selectDataProvider)) {
            $categoryList = Category::findAll(['parent_id' => 0]);
            self::$_selectDataProvider = [];

            self::$_selectDataProvider[0] = 'Выберите категорию ниже...';

            foreach ($categoryList as $category) {
                self::$_selectDataProvider[$category->id] = $category->name;
            }

            unset($categoryList);
        }

        return self::$_selectDataProvider;
    }
}
