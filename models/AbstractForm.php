<?php

namespace app\models;

use yii\base\Model;

abstract class AbstractForm extends Model
{
    public function checkCategoryId($attribute, $params)
    {
        if (empty($params)) {
            $params = [
                'strict' => false
            ];
        }

        if (is_null(Category::findOne($this->$attribute))) {
            if (!$params['strict'] && $this->$attribute == 0) {
                return;
            }

            $this->addError($attribute, 'Категория не существует');
        }
    }
}