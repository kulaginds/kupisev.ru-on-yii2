<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/reset.css',
        'https://fonts.googleapis.com/css?family=Roboto:400,700',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function addCss($css)
    {
        $this->css[] = $css;
    }

    public function addJs($js)
    {
        $this->js[] = $js;
    }
}
