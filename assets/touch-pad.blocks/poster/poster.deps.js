({
    block: 'poster',
    shouldDeps: [
        { elem: 'content' },
        { elem: 'picture' },
        { elem: 'title' },
    ]
})