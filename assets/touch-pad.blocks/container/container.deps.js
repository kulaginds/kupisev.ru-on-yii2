({
    block: 'container',
    shouldDeps: [
        { elem: 'block' },
        { elem: 'logo' },
        { elem: 'menu' },
        { elem: 'search-form' },
        { elem: 'text' },
    ]
})