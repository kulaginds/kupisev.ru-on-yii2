({
    block: 'profile',
    shouldDeps: [
        { elem: 'poster-detail' },
        { elem: 'content' },
        { elem: 'gallery' },
        { elem: 'opener' },
        { elem: 'sidebar' },
        { elem: 'sidebar-container' },
    ]
})