({
    block: 'poster-detail',
    shouldDeps: [
        { elem: 'date' },
        { elem: 'menu' },
        { elem: 'price' },
        { elem: 'small-title' },
        { elem: 'text' },
        { elem: 'title' },
    ]
})