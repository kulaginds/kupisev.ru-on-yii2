(function ($) {
    var add_img_btn = document.getElementsByClassName('add-image'),
        file_pic = document.getElementById('file_pic'),
        file_submit_btn = document.getElementById('file_submit');

    if (add_img_btn.length > 0) {
        Array.from(add_img_btn).forEach(function(item, i, arr) {
            item.addEventListener('click', function(e) {
                file_pic.click();

                e.preventDefault();
            });
        });

        document.getElementById('file_pic').addEventListener('change', function(e) {
            if (e.currentTarget.value.length > 0) {
                file_submit_btn.click();
            }
        });
    }

    $( '.swipebox' ).swipebox({
        hideCloseButtonOnMobile: true,
    });
})(jQuery);