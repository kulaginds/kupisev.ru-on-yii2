({
    block: 'gallery',
    shouldDeps: [
        { elem: 'delete' },
        { elem: 'form' },
        { elem: 'picture' },
        { elem: 'pictures' },
        { elem: 'title' },
        { block: 'swipebox' },
    ],
    mustDeps: [
        { block: 'swipebox' },
    ]
})