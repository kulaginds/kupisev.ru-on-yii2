({
    block: 'button',
    shouldDeps: [
        { elem: 'icon' },
        { mod: 'align', val: 'right' },
        { mod: 'color', val: 'blue' },
        { mod: 'color', val: 'green' },
        { mod: 'color', val: 'red' },
        { mod: 'width', val: 'full' },
    ]
})