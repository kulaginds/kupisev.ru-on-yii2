({
    block: 'poster',
    shouldDeps: [
        { elem: 'date' },
        { elem: 'price' },
        { elem: 'title' },
    ]
})