({
    block: 'menu',
    shouldDeps: [
        { elem: 'link' },
        { mod: 'align', val: 'center' },
        { mod: 'distance', val: 'large' },
        { mod: 'type', val: 'vertical' },
    ]
})