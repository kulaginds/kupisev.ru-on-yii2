({
    block: 'container',
    shouldDeps: [
        { elem: 'article' },
        { elem: 'block' },
        { elem: 'logo' },
        { elem: 'menu' },
        { elem: 'search-form' },
        { elem: 'text' },
    ]
})