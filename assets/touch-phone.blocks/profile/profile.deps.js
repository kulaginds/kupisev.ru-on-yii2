({
    block: 'profile',
    shouldDeps: [
        { elem: 'poster-detail' },
        { elem: 'content' },
        { elem: 'gallery' },
        { elem: 'header' },
        { elem: 'logo' },
        { elem: 'opener' },
        { elem: 'sidebar' },
        { elem: 'sidebar-container' },
        { mod: 'type', val: 'open' },
    ]
})