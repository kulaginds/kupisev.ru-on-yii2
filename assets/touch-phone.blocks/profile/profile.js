(function(d) {
    var opener = document.querySelector('.profile__opener'),
        profile = document.querySelector('.profile'),
        open_class = 'profile_type_open';

    opener.onclick = function() {
        if (profile.classList.contains(open_class)) {
            profile.classList.remove(open_class);
        } else {
            profile.classList.add(open_class);
        }
    };
})(document);
