({
    block: 'search-form',
    shouldDeps: [
        { elem: 'button' },
        { elem: 'textfield' },
    ]
})