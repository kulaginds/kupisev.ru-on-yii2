({
    block: 'list',
    shouldDeps: [
        { elem: 'item' },
        { elem: 'pagination' },
    ]
})