({
    block: 'form',
    shouldDeps: [
        { elem: 'button' },
        { elem: 'error' },
        { elem: 'option' },
        { elem: 'option-group' },
        { elem: 'text' },
        { elem: 'title' },
        { mod: 'type', val: 'no-title' },
    ]
})