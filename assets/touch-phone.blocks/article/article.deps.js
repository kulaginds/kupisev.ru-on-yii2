({
    block: 'article',
    shouldDeps: [
        { elem: 'button-group' },
        { elem: 'paragraph' },
        { elem: 'title' },
    ]
})