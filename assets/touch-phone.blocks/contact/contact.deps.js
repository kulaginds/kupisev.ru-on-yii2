({
    block: 'contact',
    shouldDeps: [
        { elem: 'info' },
        { elem: 'name' },
        { elem: 'picture' },
    ]
})