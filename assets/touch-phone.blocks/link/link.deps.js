({
    block: 'link',
    shouldDeps: [
        { elem: 'icon' },
        { mod: 'no', val: 'decoration' },
        { mod: 'only', val: 'icon' },
    ]
})