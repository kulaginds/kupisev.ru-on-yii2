<?php

/* @var $this yii\web\View */

?>
<div class="article container__article">
    <h1 class="article__title">Контакты</h1>
    <p class="text article__paragraph">Если вам есть что сказать или предложить, пишите на <a href="mailto:support@kupisev.ru" class="link">support@kupisev.ru</a>.</p>
</div>
