<?php

/* @var $this yii\web\View */
/* @var $model app\models\SearchForm */
/* @var $adverts array */
/* @var $paginationLinks array */
/* @var $categoryList array */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\Pagination;
use app\widgets\Form;

?>
<div class="profile__sidebar">
    <div class="profile__header">
        <?= Html::a(Yii::$app->name, ['site/index'], ['class' => 'profile__logo']) ?>
        <div class="profile__opener">
            <img class="icon" src="/images/magnifier.svg" alt="search form opener">
        </div>
    </div>
    <div class="profile__sidebar-container">
        <? $form = Form::begin([
            'action' => Url::to(['site/search']),
            'method' => 'GET',
            'options' => [
                'class' => 'form form_type_no-title',
            ],
        ]); ?>

        <?= $form->field($model, 'query')->textInput() ?>

        <?= $form->field($model, 'category_id')->dropDownList($categoryList) ?>

        <input class="button button_color_red button_width_full" type="submit" value="Поиск">

        <? Form::end(); ?>
    </div>
</div>
<div class="profile__content">
    <div class="list">
        <? if (count($adverts) > 0):
           /** @var app\models\Advert $advert */
           foreach ($adverts as $advert): ?>
        <a class="poster list__item" href="<?= Url::to(['advert/index', 'id' => $advert->id]) ?>">
            <img class="picture poster__picture" src="<?= $advert->getAvatarUrl() ?>">
            <div class="poster__content">
                <div class="poster__title"><?= $advert->title ?></div>
                <time class="poster__date"><?= Yii::$app->formatter->asDate($advert->created_at) ?></time>
                <div class="price poster__price"><?= Yii::$app->formatter->asCurrency($advert->cost) ?></div>
            </div>
        </a>
        <? endforeach; ?>
        <? if (count($paginationLinks) > 0): ?>
        <ul class="list__pagination menu menu_align_center">
            <? if (!empty($paginationLinks[Pagination::LINK_FIRST])): ?>
            <li class="menu__link">
                <a href="<?= $paginationLinks[Pagination::LINK_FIRST] ?>" class="link">Первая</a>
            </li>
            <? endif; ?>
            <? if (!empty($paginationLinks[Pagination::LINK_PREV])): ?>
            <li class="menu__link">
                <a href="<?= $paginationLinks[Pagination::LINK_PREV] ?>" class="link"><< Предыдущая</a>
            </li>
            <? endif; ?>
            <? if (!empty($paginationLinks[Pagination::LINK_NEXT])): ?>
            <li class="menu__link">
                <a href="<?= $paginationLinks[Pagination::LINK_NEXT] ?>" class="link">Следующая >></a>
            </li>
            <? endif; ?>
            <? if (!empty($paginationLinks[Pagination::LINK_LAST])): ?>
            <li class="menu__link">
                <a href="<?= $paginationLinks[Pagination::LINK_LAST] ?>" class="link">Последняя</a>
            </li>
            <? endif; ?>
        </ul>
        <? endif; ?>
        <? else: ?>
        <p class="text">По заданному запросу нет объявлений.</p>
        <? endif; ?>
    </div>
</div>
