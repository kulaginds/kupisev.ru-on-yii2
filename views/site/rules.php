<?php

/* @var $this yii\web\View */

?>
<div class="article container__article">
    <h1 class="article__title">Правила</h1>
    <p class="text article__paragraph">Объявление не должно нарушать законы РФ.</p>
    <p class="text article__paragraph">Правила могут быть изменены в любой момент без уведомления.</p>
</div>

