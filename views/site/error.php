<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

?>
<div class="article container__article">
    <h1 class="article__title">Страница не найдена</h1>
    <p class="text article__paragraph">Ошибка 404.</p>
</div>
