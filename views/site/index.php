<?php

/* @var $this yii\web\View */
/* @var $model app\models\SearchForm */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<? $form = ActiveForm::begin([
    'action' => Url::to(['site/search']),
    'method' => 'GET',
    'options' => [
        'class' => 'search-form container__search-form',
    ],
    'fieldConfig' => [
        'template' => '{input}',
        'options' => [
            'tag' => false,
        ],
    ],
]); ?>

<?= $form->field($model, 'query')->textInput([
    'class' => 'textfield search-form__textfield',
    'placeholder' => 'продам мотоцикл',
    'autofocus' => true,
]) ?>

<a class="button search-form__button button_color_green" href="javascript:this.document.forms[0].submit();">
    <img class="icon button__icon" src="/images/magnifier.svg" alt="search button">
</a>

<? ActiveForm::end() ?>

<p class="text container__text">Сервис <a class="link" href="<?= Url::to(['site/index']) ?>"><?= Yii::$app->name ?></a> позволяет найти объявления о продаже товаров или услуг в г.Севастополь.</p>
<div class="block container__block">
    <a class="button button_color_red block__button" href="<?= Url::to(['advert/create']) ?>">Подать объявление</a>
</div>
