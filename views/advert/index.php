<?php

/* @var $this yii\web\View */
/* @var $backUrl string */
/* @var $advert app\models\Advert */
/* @var $canEdit boolean */
/* @var $photos array */
/* @var $photoModel app\models\PhotoForm */

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\ContactWidget;
use app\widgets\ShareWidget;
use app\widgets\Form;

?>
<div class="profile__sidebar">
    <div class="profile__header">
        <a class="profile__logo" href="<?= $backUrl ?>">Поиск</a>
        <div class="profile__opener">
            <img class="icon" src="/images/phone-white.svg" alt="contact block opener">
        </div>
    </div>
    <div class="profile__sidebar-container">
        <div class="contact">
            <img class="picture contact__picture" src="/images/img_avatar3.png" alt="фото пользователя">
            <div class="contact__info">
                <div class="contact__name"><?= $advert->contact_name ?></div>
                <?= ContactWidget::widget(['dataProvider' => $advert->getContacts()]) ?>
            </div>
        </div>
    </div>
</div>

<div class="poster-detail profile__poster-detail">
    <h1 class="poster-detail__title"><?= $advert->title ?></h1>
    <time class="poster-detail__date">Размещено <?= Yii::$app->formatter->asDate($advert->created_at) ?></time>
    <div class="price poster-detail__price"><?= Yii::$app->formatter->asCurrency($advert->cost) ?></div>

    <h2 class="poster-detail__small-title">Описание автора</h2>
    <p class="text poster-detail__text"><?= nl2br(Html::encode($advert->description)) ?></p>

    <h2 class="poster-detail__small-title">Поделиться</h2>
    <?= ShareWidget::widget([
        'title' => $advert->title,
        'url' => Url::to(['advert/index', 'id' => $advert->id], true),
        'picture' => empty($advert->avatar) ? null : $advert->getAvatarUrl(true),
    ]) ?>

    <? if ($canEdit): ?>
    <h2 class="poster-detail__small-title">Управление объявлением</h2>
    <p class="text poster-detail__text">
        <a class="button button_color_blue" href="<?= Url::to(['advert/edit', 'id' => $advert->id, 'edit_hash' => $advert->edit_hash]) ?>">Редактировать</a>
        <a class="button button_color_red" href="<?= Url::to(['advert/delete', 'id' => $advert->id, 'delete_hash' => $advert->delete_hash]) ?>">Удалить</a>
    </p>
    <? endif; ?>
</div>

<div class="gallery profile__gallery">
    <div class="gallery__title">Фото</div>
    <? if ($canEdit): ?>
        <a href="#" class="button button_color_green add-image">Добавить</a>
        <? $form = Form::begin([
            'action' => Url::to(['photo/upload', 'id' => $advert->id]),
            'options' => [
                'class' => 'file-upload gallery__form',
            ],
            'fieldConfig' => [
                'template' => '{input}',
                'options' => [
                    'class' => null,
                ],
            ],
        ]); ?>
        <?= $form->field($photoModel, 'imageFile')
                 ->label(false)->fileInput(['id' => 'file_pic']) ?>
        <input type="hidden" name="MAX_FILE_SIZE" value="2097152">
        <input type="submit" id="file_submit">
        <? Form::end() ?>
    <? endif; ?>
    <div class="gallery__pictures">
        <? if (count($photos) > 0):
           /** @var app\models\Photo $photo */
           foreach ($photos as $photo): ?>
        <div class="gallery__picture">
            <a href="<?= $photo->getUrl() ?>" class="swipebox">
                <img class="picture" src="<?= $photo->getThumbUrl() ?>" >
            </a>
            <? if ($canEdit): ?>
            <a class="link link_only_icon gallery__delete" href="<?= Url::to(['photo/delete',
                'id' => $advert->id, 'photo_id' => $photo->id]) ?>">
                <img class="link__icon" src="/images/delete.svg" alt="delete button">
            </a>
            <? endif; ?>
        </div>
        <? endforeach;
           else: ?>
        <p class="text">Нет фото.</p>
        <? endif; ?>
    </div>
</div>
