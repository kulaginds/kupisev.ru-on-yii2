<?php

/* @var $this yii\web\View */
/* @var $advert app\models\Advert */
/* @var $error boolean */

use yii\helpers\Url;

?>
<div class="article container__article">
    <h1 class="article__title">Удалить объявление</h1>
    <? if ($error): ?>
    <p class="text article__paragraph">Не удалось удалить объявление...</p>
    <p class="text article__paragraph"><a href="<?= Url::to(['advert/index',
            'id' => $advert->id]) ?>" class="link">Назад</a></p>
    <? else: ?>
    <p class="text article__paragraph">Вы уверены, что хотите удалить объявление?</p>
    <div class="article__button-group">
        <a class="button button_color_red" href="<?= Url::to(['advert/delete',
            'id' => $advert->id, 'delete_hash' => $advert->delete_hash,
            'confirm' => true]) ?>">Удалить</a>
        <a class="button button_align_right" href="<?= Url::to(['advert/index',
            'id' => $advert->id]) ?>">Назад</a>
    </div>
    <? endif; ?>
</div>
