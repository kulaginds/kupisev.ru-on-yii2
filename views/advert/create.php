<?php

/* @var $this yii\web\View */
/* @var $edit boolean */
/* @var $model \app\models\AdvertForm */
/* @var $categoryList array */

use app\widgets\Form;
use yii\helpers\Url;
use himiklab\yii2\recaptcha\ReCaptcha;

?>
<div class="article container__article">
    <h1 class="article__title"><?= $this->title ?></h1>
    <? $form = Form::begin() ?>

        <?= $form->field($model, 'title')
                 ->textInput() ?>

        <?= $form->field($model, 'cost')
                 ->textInput(['min' => 0, 'type' => 'number']) ?>

        <?= $form->field($model, 'category_id')->dropDownList($categoryList) ?>

        <?= $form->field($model, 'description')
                 ->textarea(['cols' => 30, 'rows' => 10]) ?>

        <?= $form->field($model, 'contact_name')
                 ->textInput() ?>

        <?= $form->field($model, 'contact_email')
                 ->textInput() ?>

        <?= $form->field($model, 'contact_phone')
                 ->textInput() ?>

        <? if (!$edit): ?>
        <?= $form->field($model, 'reCaptcha')->widget(ReCaptcha::class)
                 ->label(false) ?>
        <? endif; ?>

        <? if ($edit): ?>
            <input class="button button_color_red form__button" type="submit" value="Сохранить объявление">
            <a class="button button_align_right form__button" href="<?= Url::to(['advert/index', 'id' => $model->getAdvert()->id]) ?>">Назад</a>
        <? else: ?>
            <p class="text form__text">Нажимая кнопку "Создать объявление" вы соглашаетесь с <a class="link" href="/rules" target="_blank">правилами сайта</a>.</p>
            <input class="button button_color_red form__button" type="submit" value="Создать объявление">
            <input class="button button_align_right form__button" type="reset" value="Очистить форму">
        <? endif; ?>
    <? Form::end(); ?>
</div>
