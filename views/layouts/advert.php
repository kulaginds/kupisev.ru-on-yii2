<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\DetailAsset;

DetailAsset::register($this);

$this->title = Yii::$app->name;

if (!isset($this->description)) {
    $description = Yii::$app->params['defaultDescription'];
}

$this->registerMetaTag([
    'name' => 'description',
    'content' => $description,
]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter49794901 = new Ya.Metrika2({ id:49794901, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/49794901" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="/images/logo/16x16.png" type="image/png">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/logo/57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/logo/72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/logo/60x60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/logo/76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/logo/114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/logo/120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/logo/144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/logo/152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/logo/180x180.png">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="profile">
    <?= $content ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
