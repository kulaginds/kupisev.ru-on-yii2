<?php

/* @var $this yii\web\View */
/* @var $photos array */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Редактирование объявления';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">

    <div class="body-content">

        <h1><?= $this->title ?></h1>
        <p>&nbsp;</p>
        <? $form = ActiveForm::begin([
            'id' => 'project-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-sm-4\">{input}</div>\n<div class=\"col-lg-4\">{error}</div>",
            ],
        ]); ?>

            <div class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="row">
                        <h3>Загруженные фото</h3>
                        <? if (count($photos) > 0):
                            /** @var app\models\Photo $photo */
                            foreach ($photos as $photo): ?>
                                <div class="thumbnail col-md-2">
                                    <img src="<?= $photo->getThumbUrl() ?>">
                                </div>
                            <? endforeach;
                        else: ?>
                            <p>Загруженных фото нет</p>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <h3>Фото для загрузки</h3>
                    <div class="row" id="photos"></div>
                </div>
            </div>

            <?= $form->field($model, 'title')
                ->textInput() ?>

            <?= $form->field($model, 'cost')
                ->textInput(['min' => 0, 'type' => 'number']) ?>

            <?= $form->field($model, 'category_id')->dropDownList($categoryList) ?>

            <?= $form->field($model, 'description')
                ->textarea(['cols' => 30, 'rows' => 10]) ?>

            <?= $form->field($model, 'contact_name')
                ->textInput() ?>

            <?= $form->field($model, 'contact_email')
                ->textInput() ?>

            <?= $form->field($model, 'contact_phone')
                ->textInput() ?>

            <?= $form->field($model, 'contact_vk')
                ->textInput() ?>

            <div class="row">
                <div class="col-sm-offset-3">
                    <?= Html::submitButton('Сохранить и проверить', ['class' => 'btn btn-primary', 'name' => 'add-project']) ?>
                    <?= Html::a('Удалить', ['admin/delete', 'id' => $advert->id]) ?>
                </div>
            </div>

        <? ActiveForm::end(); ?>
    </div>
</div>
<script src="https://vk.com/js/api/openapi.js?157" type="text/javascript"></script>
<script>
    function init() {
        var name = document.getElementById('advertform-contact_name'),
            description = document.getElementById('advertform-description'),
            vk = document.getElementById('advertform-contact_vk'),
            photos = document.getElementById('photos');

        if (description.innerHTML.length > 0) {
            var text = description.innerHTML,
                match = null;

            if (match = text.match(/author_id:(\d+)/)) {
                description.innerHTML = text = text.replace(match[0] + "\n", '');
                var vk_id = match[1];
                VK.Api.call('users.get', { user_ids:vk_id, v:"5.73" }, function(result) {
                    if (!result.response) {
                        return;
                    }

                    var first_name = result.response[0].first_name,
                        last_name = result.response[0].last_name;

                    name.value = (first_name + ' ' + last_name).trim();

                    if (match = text.match(/post_id:(\d+)/)) {
                        description.innerHTML = text = text.replace(match[0] + "\n", '');
                        VK.Api.call('wall.getById', { posts: vk_id + '_' + match[1], v: '5.73' }, function(result) {
                            if (result.response.length > 0) {
                                vk.value = 'https://vk.com/wall' + vk_id + '_' + match[1];
                            } else {
                                vk.value = 'удалён';
                            }
                        });
                    }

                    description.innerHTML = text = text.trim();
                });
            }

            if (match = text.match(/photo:(.+)/ig)) {
                match.forEach(function (photo, index, arr) {
                    description.innerHTML = text = text.replace(photo + "\n", '');
                    photo = photo.replace('photo:', '');
                    url = '<?= Url::to(['admin/upload', 'id' => $advert->id, 'url' => 'url']) ?>';
                    url = url.substr(0, url.length - 3) + photo;
                    photos.innerHTML += '<a href="' + url + '" class="thumbnail col-md-2">\n' +
                    '    <img src="' + photo + '">\n' +
                    '</a>';
                });
            }

            description.innerHTML = text = text.trim();
        }
    }

    VK.init({
        apiId: 6632651
    });
    init();
</script>
