<?php

/* @var $this yii\web\View */
/* @var $adverts array */
/* @var $pages \yii\data\Pagination */

use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Непроверенные объявления';

?>
<div class="site-index">

    <div class="body-content">

        <h1><?= $this->title ?></h1>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Описание</th>
                <th width="100">Действия</th>
            </tr>
            </thead>
            <tbody>
            <? if (count($adverts) > 0):
               /** @var app\models\Advert $advert */
               foreach($adverts as $advert): ?>
                    <tr>
                        <td><a href="<?= Url::to(['admin/edit', 'id' => $advert->id]) ?>" class="btn btn-link"><?= StringHelper::truncate($advert->getAdminDescription(), 100) ?></a></td>
                        <td>
                            <a href="<?= Url::to(['admin/delete', 'id' => $advert->id]) ?>" class="btn btn-danger">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </td>
                    </tr>
                <? endforeach; ?>
            <? else: ?>
                <tr>
                    <td colspan="2">
                        <p>Нет непроверенных объявлений.</p>
                    </td>
                </tr>
            <? endif; ?>
            </tbody>
        </table>

        <?= LinkPager::widget([
            'pagination' => $pages,
        ]) ?>

    </div>
</div>
