<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'kupisev',
    'name' => 'KupiSev.ru',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'iVYFQalbN83jU91mo_VJpYozZhK6H-Tn',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'loginUrl' => ['admin/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/'                                            => 'site/index',
                '/rules'                                       => 'site/rules',
                '/about-us'                                    => 'site/about-us',
                '/contacts'                                    => 'site/contacts',
                '/search'                                      => 'site/search',
                '/advert/<id:\d+>'                             => 'advert/index',
                '/advert/create'                               => 'advert/create',
                '/advert/<id:\d+>/edit/<edit_hash:\w+>'        => 'advert/edit',
                '/advert/<id:\d+>/delete/<delete_hash:\w+>'    => 'advert/delete',
                '/advert/<id:\d+>/photo/upload'                => 'photo/upload',
                '/advert/<id:\d+>/photo/<photo_id:\d+>/delete' => 'photo/delete',
            ],
        ],
        'formatter' => [
            'locale' => 'ru-RU',
            'numberFormatterOptions' => [
                NumberFormatter::MAX_FRACTION_DIGITS => 0,
            ]
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => $params['recaptchaPublic'],
            'secret' => $params['recaptchaPrivate'],
        ],
        'mailqueue' => [
            'class' => 'nterms\mailqueue\MailQueue',
            'table' => '{{%mail_queue}}',
            'mailsPerRound' => 10,
            'maxAttempts' => 3,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
