<?php

return [
    'class' => 'Swift_SmtpTransport',
    'host' => 'localhost',
    'username' => 'username',
    'password' => 'password',
    'port' => '25',
];
