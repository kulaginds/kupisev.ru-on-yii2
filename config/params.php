<?php

$params_secure = require __DIR__ . '/params_secure.php';

return array_merge([
    'adminEmail' => 'admin@kupisev.ru',
    'noreplyEmail' => 'no-reply@kupisev.ru',
    'searchPageSize' => 30,
    'uploadsPath' => realpath(__DIR__ . '/../web/uploads') . '/',
    'uploadsDir' => '/uploads/',
    'defaultAvatar' => '/images/default.png',
    'advertLifetime' => 7, // days
    'checkAfterCreate' => true,
    'defaultDescription' => 'Сервис KupiSev.ru позволяет найти объявления о продаже товаров или услуг.',
], $params_secure);
