<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;
use app\models\AdvertContact;

class AdvertContactFixture extends ActiveFixture
{
    public $modelClass = AdvertContact::class;
}