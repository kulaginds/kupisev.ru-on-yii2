<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;
use app\models\Advert;

class AdvertFixture extends ActiveFixture
{
    public $modelClass = Advert::class;
    public $depends = [
        AdvertContactFixture::class,
    ];
}