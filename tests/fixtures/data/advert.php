<?php

return [
    [
        'id' => 1,
        'title' => 'Мотоцикл Honda CB125',
        'description' => 'Кому мало 5 км/ч, разбирайте.',
        'cost' => 100000,
        'category_id' => 4,
        'edit_hash' => 0x46fb071558c53f2118e5f54a46fbc48c,
        'delete_hash' => 0x81cfc8e6176dc2e5fb18c9d08fec6176,
        'contact_name' => 'Афанасий',
        'updated_at' => date('Y-m-d H:i:s'),
        'created_at' => date('Y-m-d H:i:s'),
        'avatar' => NULL,
        'checked' => 1,
    ]
];
