<?php

namespace app\controllers;

use app\models\Photo;
use app\models\PhotoForm;
use Yii;
use app\models\Advert;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class PhotoController extends AbstractController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'upload' => ['post'],
                ],
            ],
        ];
    }

    public function actionUpload($id)
    {
        $advert = Advert::findOne($id);

        if (is_null($advert)) {
            return $this->redirect(['site/index']);
        }

        if (!$this->hasPermissions($advert)) {
            return $this->redirect(['advert/index', 'id' => $id]);
        }

        if (Yii::$app->request->isPost) {
            $model = new PhotoForm(['id' => $advert->id, 'scenario' => 'file']);
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->upload();
        }

        return $this->redirect(['advert/index', 'id' => $id]);
    }

    public function actionDelete($id, $photo_id)
    {
        $advert = Advert::findOne($id);
        $photo = Photo::findOne(['advert_id' => $id, 'id' => $photo_id]);

        if (is_null($advert) || is_null($photo)) {
            return $this->redirect(['site/index']);
        }

        if (!$this->hasPermissions($advert)) {
            return $this->redirect(['advert/index', 'id' => $id]);
        }

        $photo->delete();

        return $this->redirect(['advert/index', 'id' => $id]);
    }
}