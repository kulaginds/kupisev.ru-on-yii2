<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class AbstractController extends Controller
{
    protected function addEditHash($edit_hash)
    {
        $hashes = Yii::$app->session->get('hashes', []);

        if (in_array($edit_hash, $hashes)) {
            return;
        }

        $hashes[] = $edit_hash;

        Yii::$app->session->set('hashes', $hashes);
    }

    /** @var $advert Advert */
    protected function hasPermissions($advert)
    {
        return in_array($advert->edit_hash,
            Yii::$app->session->get('hashes', []));
    }
}