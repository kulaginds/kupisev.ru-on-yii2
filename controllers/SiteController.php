<?php

namespace app\controllers;

use app\models\Advert;
use app\models\Category;
use Yii;
use app\models\SearchForm;
use yii\data\Pagination;
use yii\web\Controller;
use yii\helpers\Url;

class SiteController extends Controller
{
    public function actionError()
    {
        return $this->render('error');
    }

    public function actionIndex()
    {
        $model = new SearchForm();

        return $this->render('index', compact('model'));
    }

    public function actionRules()
    {
        return $this->render('rules');
    }

    public function actionAboutUs()
    {
        return $this->render('about-us');
    }

    public function actionContacts()
    {
        return $this->render('contacts');
    }

    public function actionSearch()
    {
        $this->layout = 'search';
        $model = new SearchForm();
        $query = Advert::find();
        $categoryList = Category::selectDataProvider();

        Url::remember('', 'backUrl');

        if ($model->load(Yii::$app->request->get())) {
            $model->search($query);
        }

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => Yii::$app->params['searchPageSize'],
        ]);

        $adverts = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $paginationLinks = $pages->getLinks();

        return $this->render('search', compact('model', 'adverts',
            'paginationLinks', 'categoryList'));
    }
}
