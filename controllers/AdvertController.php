<?php

namespace app\controllers;

use Yii;
use app\models\AdvertForm;
use app\models\Category;
use app\models\Advert;
use app\models\PhotoForm;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class AdvertController extends AbstractController
{
    public function actionIndex($id)
    {
        $this->layout = 'advert';
        $backUrl = Url::previous('backUrl');
        $advert = Advert::findCheckedOne($id);

        if (is_null($advert)) {
            throw new NotFoundHttpException();
        }

        if (is_null($backUrl)) {
            $backUrl = Url::to(['site/search']);
        }

        $canEdit = $this->hasPermissions($advert);
        $photos = $advert->getPhotos();
        $photoModel = new PhotoForm();

        return $this->render('index', compact('backUrl', 'advert',
            'canEdit', 'photos', 'photoModel'));
    }

    public function actionCreate()
    {
        $edit = false;
        $this->view->title = 'Подать объявление';
        $model = new AdvertForm(['scenario' => 'create']);
        $categoryList = Category::selectDataProvider();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->addEditHash($model->getAdvert()->edit_hash);

            return $this->redirect(['advert/index', 'id' => $model->getAdvert()->id]);
        }

        return $this->render('create', compact('edit', 'model', 'categoryList'));
    }

    public function actionEdit($id, $edit_hash)
    {
        $edit = true;
        $this->view->title = 'Редактирование объявления';
        $advert = Advert::findOne(['id' => $id, 'edit_hash' => hex2bin($edit_hash)]);

        if (is_null($advert)) {
            throw new NotFoundHttpException();
        }

        if (!$this->hasPermissions($advert)) {
            $this->addEditHash($advert->edit_hash);
        }

        $model = new AdvertForm(['scenario' => 'edit']);
        $model->setAdvert($advert);
        $categoryList = Category::selectDataProvider();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['advert/index', 'id' => $advert->id]);
        }

        return $this->render('create', compact('edit', 'model', 'categoryList'));
    }

    public function actionDelete($id, $delete_hash, $confirm = false)
    {
        $this->view->title = 'Удаление объявления';
        $advert = Advert::findOne(['id' => $id, 'delete_hash' => hex2bin($delete_hash)]);
        $error = false;

        if (is_null($advert)) {
            throw new NotFoundHttpException();
        }

        if ($confirm) {
            if ($advert->delete()) {
                return $this->redirect(['site/index']);
            }

            $error = true;
        }

        return $this->render('delete', compact('advert', 'error'));
    }
}