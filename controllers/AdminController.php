<?php

namespace app\controllers;

use app\models\Advert;
use app\models\AdvertForm;
use app\models\Category;
use app\models\LoginForm;
use app\models\PhotoForm;
use igogo5yo\uploadfromurl\UploadFromUrl;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AdminController extends Controller
{
    public $layout = 'admin';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'index', 'edit', 'upload', 'delete'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'edit', 'upload', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $query = Advert::find()
                    ->where(['checked' => false]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
        ]);
        $adverts = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', compact('adverts', 'pages'));
    }

    public function actionEdit($id)
    {
        $advert = Advert::find()
                    ->where(['id' => $id, 'checked' => false])
                    ->one();

        if (is_null($advert)) {
            return $this->redirect(['admin/index']);
        }

        $model = new AdvertForm();
        $model->setAdvert($advert);
        $categoryList = Category::selectDataProvider();
        $photos = $advert->getPhotos();

        if ($model->load(Yii::$app->request->post()) && $model->saveAndCheck()) {
            return $this->redirect(['admin/index']);
        }

        return $this->render('edit', compact('advert', 'model',
            'categoryList', 'photos'));
    }

    public function actionUpload($id, $url)
    {
        $advert = Advert::find()
                    ->where(['id' => $id, 'checked' => false])
                    ->one();

        if (is_null($advert)) {
            return $this->redirect(['admin/index']);
        }

        $model = new PhotoForm(['id' => $id, 'scenario' => 'url']);
        $model->imageFile = UploadFromUrl::initWithUrl($url);
        $model->upload();

        return $this->redirect(['admin/edit', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        $advert = Advert::findOne($id);

        if (!is_null($advert)) {
            $advert->delete();
        }

        return $this->redirect(['admin/index']);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['admin/index']);
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['admin/index']);
    }
}