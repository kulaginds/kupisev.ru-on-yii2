<?php

namespace app\widgets;

use app\models\AdvertContact;
use yii\base\Widget;

class ContactWidget extends Widget
{
    public $dataProvider;

    public function run()
    {
        $result = '<ul class="menu menu_type_vertical">';

        /** @var AdvertContact $contact */
        foreach ($this->dataProvider as $contact) {
            $result .= '<li class="menu__link">';

            switch ($contact->type) {
                case 'email':
                    $result .= '<a class="link" href="mailto:' . $contact->value . '">
                            <img class="icon link__icon" src="/images/email.svg" alt="email icon">
                            ' . $contact->value . '
                        </a>';
                    break;
                case 'phone':
                    $result .= '<a class="link" href="tel:' . $contact->value . '">
                            <img class="icon link__icon" src="/images/phone.svg" alt="phone icon">
                            ' . $contact->value . '
                        </a>';
                    break;
                case 'vk':
                    $result .= '<a class="link" href="' . $contact->value . '">
                            <img class="icon link__icon" src="/images/vk.svg" alt="vk icon">
                            ' . substr($contact->value, 15) . '
                        </a>';
                    break;
            }

            $result .= '</li>';
        }

        $result .= '</ul>';

        return $result;
    }
}