<?php

namespace app\widgets;

class Form extends \yii\widgets\ActiveForm
{
    public $options = [
        'class' => 'form',
    ];

    public $fieldClass = 'app\widgets\Field';

    public $fieldConfig = [];
}