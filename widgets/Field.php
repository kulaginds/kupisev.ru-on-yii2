<?php

namespace app\widgets;

use yii\widgets\ActiveField;

class Field extends ActiveField
{
    public $options = [
        'class' => 'form__option-group'
    ];

    public $template = "{label}\n<div class=\"form__option\">{input}</div>\n{error}";

    public $inputOptions = [];

    public $errorOptions = ['class' => 'form__error'];

    public $labelOptions = ['class' => 'form__title'];

    public function textInput($options = [])
    {
        $options['class'] = 'textfield';

        return parent::textInput($options);
    }

    public function textarea($options = [])
    {

        $options['class'] = 'textarea';

        return parent::textarea($options);
    }

    public function dropDownList($items, $options = [])
    {
        $options['class'] = 'combobox';

        return parent::dropDownList($items, $options);
    }
}