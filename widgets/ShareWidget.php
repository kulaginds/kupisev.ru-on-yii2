<?php

namespace app\widgets;

use yii\base\Widget;

class ShareWidget extends Widget
{
    public $title;
    public $url;
    public $picture;

    protected $_socials = ['vk', 'facebook', 'twitter'];

    public function run()
    {
        $result = '<ul class="menu poster-detail__menu">';

        foreach ($this->_socials as $social) {
            switch ($social) {
                case 'vk':
                    $url = $this->getVkLink();
                    break;
                case 'facebook':
                    $url = $this->getFacebookLink();
                    break;
                case 'twitter':
                    $url = $this->getTwitterLink();
                    break;
            }

            $result .= '<li class="menu__link">
            <a href="' . $url . '" target="_blank" class="link link_no_decoration">
                <img src="/images/' . $social . '.svg" class="icon link__icon">
            </a>
        </li>';
        }

        $result .= '</ul>';

        return $result;
    }

    protected function getVkLink()
    {
        $url = 'https://vk.com/share.php?';
        $url .= 'title=' . urlencode($this->title);
        $url .= '&url=' . urlencode($this->url);

        if (!empty($this->picture)) {
            $url .= '&image=' . urlencode($this->picture);
        }

        return $url;
    }

    protected function getFacebookLink()
    {
        $url = 'http://www.facebook.com/sharer.php?s=100';
        $url .= '&p[title]=' . urlencode($this->title);
        $url .= '&p[url]=' . urlencode($this->url);

        if (!empty($this->picture)) {
            $url .= '&p[images][0]=' . urlencode($this->picture);
        }

        return $url;
    }

    protected function getTwitterLink()
    {
        $url = 'http://twitter.com/share?';
        $url .= 'text=' . urlencode($this->title);
        $url .= '&url=' . urlencode($this->url);

        return $url;
    }
}